<?php

namespace App\Http\Controllers;

use Image;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManager;

class ImageController extends Controller
{
    function UploadImage(Request $request){

        $path = public_path()."/images/Service/";
        if($request->hasFile('profile_image')) {
            $image = $request->file('profile_image');
            $img = Image::make($image->getRealPath());


            //get filename with extension
            $filenamewithextension = $request->file('profile_image')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('profile_image')->getClientOriginalExtension();
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$filename.'.'.$extension);
            //get file extension


            //filename to store
//            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
//            $request->file('profile_image')->move($path, $filenametostore);
//            $image->move($path, $filenametostore);
        }

        return view('welcome');
    }
}
